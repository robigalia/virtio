# virtio

[![Crates.io](https://img.shields.io/crates/v/virtio.svg?style=flat-square)](https://crates.io/crates/virtio)

[Documentation](https://doc.robigalia.org/virtio)

A platform-agnostic [virtio
1.0](http://docs.oasis-open.org/virtio/virtio/v1.0/virtio-v1.0.html) driver.

## Status

Incomplete.
