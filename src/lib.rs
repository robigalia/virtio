/* Copyright (c) 2015 The Robigalia Project Developers
 * Licensed under the Apache License, Version 2.0
 * <LICENSE-APACHE or
 * http://www.apache.org/licenses/LICENSE-2.0> or the MIT
 * license <LICENSE-MIT or http://opensource.org/licenses/MIT>,
 * at your option. All files in the project carrying such
 * notice may not be copied, modified, or distributed except
 * according to those terms.
 */

#![feature(no_std)]
#![no_std]

macro_rules! le {
    ($name:ident, $prim:ident) => (
        #[repr(C)]
        pub struct $name($prim);
        impl $name {
            pub fn set(&mut self, val: $prim) {
                self.0 = val.to_le();
            }
            pub fn get(&self) -> $prim {
                $prim::from_le(self.0)
            }
        }
    )
}

le!(le16, u16);
le!(le32, u32);
le!(le64, u64);

pub mod virtqueue;
mod device;

#[cfg(feature = "pci")]
pub mod pci;
#[cfg(feature = "mmio")]
pub mod mmio;
#[cfg(feature = "channel")]
pub mod channel;
