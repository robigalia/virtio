/* Copyright (c) 2015 The Robigalia Project Developers
 * Licensed under the Apache License, Version 2.0
 * <LICENSE-APACHE or
 * http://www.apache.org/licenses/LICENSE-2.0> or the MIT
 * license <LICENSE-MIT or http://opensource.org/licenses/MIT>,
 * at your option. All files in the project carrying such
 * notice may not be copied, modified, or distributed except
 * according to those terms.
 */

// Section 2.4 of the spec

use {le16, le32, le64};

pub struct VirtQueue {
    queue_size: usize,
    // these (typed) pointers are into a contiguous buffer. we keep them separately both to avoid
    // casts all over the place but also to not have to do extra offsetting to access the rings.
    descriptor_table: *mut Descriptor,
    available_ring: *mut AvailableRing,
    used_ring: *mut UsedRing,
}

pub const DESC_FLAG_NEXT: u16 = 1;
pub const DESC_FLAG_WRITE: u16 = 2;
pub const DESC_FLAG_INDIRECT: u16 = 4;

#[repr(C)]
pub struct Descriptor {
    addr: le64,
    len: le32,
    // use bitflags when it's no_std
    flags: le16,
    next: le16,
}

pub const ARING_FLAG_NO_INTERRUPT: u16 = 1;

#[repr(C)]
pub struct AvailableRing {
    // use bitflags when it's no_std
    flags: le16,
    idx: le16,
    ring: [le16],
    /* note: there is a "used_event" field as the last element of this array iff EVENT_IDX is
     * negotiated */
}

pub const URING_FLAG_NO_INTERRUPT: u16 = 1;

#[repr(C)]
pub struct UsedRing {
    // use bitflags when it's no_std
    flags: le16,
    idx: le16,
    ring: [UsedElem],
}

#[repr(C)]
pub struct UsedElem {
    id: le32,
    len: le32,
}
