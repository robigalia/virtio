/* Copyright (c) 2015 The Robigalia Project Developers
 * Licensed under the Apache License, Version 2.0
 * <LICENSE-APACHE or
 * http://www.apache.org/licenses/LICENSE-2.0> or the MIT
 * license <LICENSE-MIT or http://opensource.org/licenses/MIT>,
 * at your option. All files in the project carrying such
 * notice may not be copied, modified, or distributed except
 * according to those terms.
 */

use virtqueue::VirtQueue;

trait Device {
    /// Initialize a device, doing feature negotiation.
    ///
    /// To do feature negotiation, `features_understood` is iterated over. For each 32-bit value,
    /// the appropriate index is passed to .feature_select() and the value is passed to
    /// .driver_features(). .device_features() is read, and that value is stored in
    /// features_understood.
    ///
    /// If all features understood are able to be negotiated, the boolean in the `Ok` result is
    /// true. Otherwise, it is false. This is not an error, but an optimization so that driver does
    /// not need to scan features_understood to see which features aren't understood.
    fn initialize(&mut self, features_understood: &mut [u32]) -> Result<bool, ()>;

    /// Perform a memory barrier.
    fn barrier(&self);

    /// Bitwise-or the bits provided with the current status bits.
    fn set_status_bits(&mut self, bits: u8);

    /// Reset the device.
    fn reset(&mut self);

    fn get_virtqueues<'a>(&'a self) -> &'a [VirtQueue];
}
